//
//  PlaceCustomTableViewCell.swift
//  OUTerest
//
//  Created by Adem Hadrovic on 4/19/18.
//  Copyright © 2018 DATTeam. All rights reserved.
//

import UIKit
import GooglePlaces

class PlaceCustomTableViewCell: UITableViewCell{
    
 
    @IBOutlet weak var placeTypeImageView: UIImageView!
    @IBOutlet weak var placeNameLabel: UILabel!
    @IBOutlet weak var placeRatingLabel: UILabel!
    @IBOutlet weak var placePriceLevelLabel: UILabel!
    @IBOutlet weak var placeVisitedStampImageView: UIImageView!
    @IBOutlet weak var placeDistanceLabel: UILabel!
    

    var currentPlace:Place = Place(placeName: "", placeAddress: "", placeRating: 0.0, placePriceLevel: 0, placeDescription: "", placeType: "",placeLatitude:0.0,placeLongitude:0.0,saved:false)
 
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        /*
        placeVisitedStampImageView.isHidden = true
        let currentHour = Calendar.current.component(.hour,from:Date())
        
         if((currentHour >= 19 || currentHour < 3) || (currentHour >= 17 && currentHour < 19 )){
            
            if currentPlace.placeType == "food"{
                placeTypeImageView.image = #imageLiteral(resourceName: "restaurant_icon_selected")
            }
            else if currentPlace.placeType == "bar"{
                placeTypeImageView.image = #imageLiteral(resourceName: "bar_icon_selected")
            }
            else if currentPlace.placeType == "cafe"{
                placeTypeImageView.image = #imageLiteral(resourceName: "cafe_icon_selected")
            }
            else if currentPlace.placeType == "bank"{
                placeTypeImageView.image = #imageLiteral(resourceName: "bank_icon_selected")
            }
            else if currentPlace.placeType == "shopping_mall"{
                placeTypeImageView.image = #imageLiteral(resourceName: "shopping_icon_selected")
            }
            else if currentPlace.placeType == "lodging"{
                placeTypeImageView.image = #imageLiteral(resourceName: "hotel_icon_selected")
            }
            else if currentPlace.placeType == "pharmacy"{
                placeTypeImageView.image = #imageLiteral(resourceName: "hospital_icon_selected")
            }
            else if currentPlace.placeType == "transit_station"{
                placeTypeImageView.image = #imageLiteral(resourceName: "transport_icon_selected")
            }
            
        }
         else{
            
            if currentPlace.placeType == "food"{
                placeTypeImageView.image = #imageLiteral(resourceName: "restaurant_icon")
            }
            else if currentPlace.placeType == "bar"{
                placeTypeImageView.image = #imageLiteral(resourceName: "bar_icon")
            }
            else if currentPlace.placeType == "cafe"{
                placeTypeImageView.image = #imageLiteral(resourceName: "cafe_icon")
            }
            else if currentPlace.placeType == "bank"{
                placeTypeImageView.image = #imageLiteral(resourceName: "bank_icon")
            }
            else if currentPlace.placeType == "shopping_mall"{
                placeTypeImageView.image = #imageLiteral(resourceName: "shopping_icon")
            }
            else if currentPlace.placeType == "lodging"{
                placeTypeImageView.image = #imageLiteral(resourceName: "hotel_icon")
            }
            else if currentPlace.placeType == "pharmacy"{
                placeTypeImageView.image = #imageLiteral(resourceName: "hospital_icon")
            }
            else if currentPlace.placeType == "transit_station"{
                placeTypeImageView.image = #imageLiteral(resourceName: "transport_icon")
            }
            
        }
        
        if currentPlace.saved {
            placeVisitedStampImageView.isHidden = false
        }
         */
 
        
        
        
    }
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    
}
