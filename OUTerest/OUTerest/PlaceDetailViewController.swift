//
//  PlaceDetailViewController.swift
//  OUTerest
//
//  Created by Adem Hadrovic on 4/19/18.
//  Copyright © 2018 DATTeam. All rights reserved.
//

import UIKit
import MapKit
import GooglePlaces
import GoogleMaps
import Alamofire
import SwiftyJSON

class PlaceDetailViewController: UIViewController {
    
    //will need to add an image
    @IBOutlet weak var descriptionButton: UIButton!
    
    @IBOutlet weak var openDirectionsButton: UIButton!
    @IBOutlet weak var placeNameLabel: UILabel!
    @IBOutlet weak var placeAddressLabel: UITextView!
    
    @IBOutlet weak var placeRatingLabel: UILabel!
    @IBOutlet weak var placePriceLevelLabel: UILabel!
    
    @IBOutlet weak var placeMapView: MKMapView!
    
    var userLat:CLLocationDegrees = 43.8597248
    var userLong:CLLocationDegrees = 18.4290523
    var lat:CLLocationDegrees = 0.0
    var lng:CLLocationDegrees = 0.0
    var savedPlaceParams = [:] as [String:Any]
    
    
    
    @IBOutlet weak var saveButton: UIButton!
    
    func configureView() {
        // Update the user interface for the place details
        if let place = detailPlace {
            if let label = placeNameLabel {
                label.text = place.placeName
            }
            if let label = placeAddressLabel {
                label.text! = place.placeAddress
            }
            if let label = placeRatingLabel {
                label.text = place.placeRating
            }
            if let label = placePriceLevelLabel {
                label.text = place.placePriceLevel
                
                
            }
            
            lat = place.placeLatitude
            lng = place.placeLongitude
            
            let latitudinalMeters: CLLocationDistance = 80
            let longitudinalMeters: CLLocationDistance = 80
            let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lng)
            
            let region = MKCoordinateRegionMakeWithDistance( coordinate, latitudinalMeters, longitudinalMeters)
            
            let annotation = MKPointAnnotation()
            annotation.coordinate = coordinate
            
            self.placeMapView.setRegion(region, animated: true)
            self.placeMapView.addAnnotation(annotation)
            
            savedPlaceParams = [
                
                "placeName":place.placeName,
                "placeAddress":place.placeAddress,
                "placeRating": place.placeRating,
                "placePriceLevel": place.placePriceLevel,
                "placeLatitude": place.placeLatitude,
                "placeLongitude": place.placeLongitude,
                "saved": "true"
                
            ]
        
            print(place.placeLatitude)
            print(place.placeLongitude)
            
        }
        
    }
    
    var detailPlace: Place? {
        didSet {
            // Update the view.
            configureView()
        }
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        //set element values here
        /*
        placeImageElement.image = placeImage
        placeAddressElement.text = placeAddress
        placeRatingElement.text = placeRating
        */
        descriptionButton.layer.cornerRadius = 10
        openDirectionsButton.layer.cornerRadius = 10
        placeMapView.layer.cornerRadius = 10
        
        
        configureView()
        //self.view.backgroundColor = UIColor.white.withAlphaComponent(0.2)
        self.showAnimate()
        
        //placeMapView.delegate = self as! MKMapViewDelegate
        
        /*
        let url = "https://emalocalserver.herokuapp.com/savedPlaces/"
        Alamofire.request(url, method: .get).validate(statusCode: 200..<600).responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                if json["status"] == "found"{
                    
                }
                
                
            case .failure(let error):
                print(error)
            }
        }*/
 

        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func closePopUp(_ sender: Any) {
        
        //simply remove the popup vc from the parent/super vc (the main vc with the show button)
        //self.view.removeFromSuperview()
        self.removeAnimate()
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
    
    //to do: add place to saved array on server
    //add saved attribute to Place Class
    // add display check mark of place visted on table
    // add saved emblem to detail screen
    
    @IBAction func showDescriptionPopUp(_ sender: UIButton){
        
        
        let popUpVc = UIStoryboard(name:"Main",bundle:nil).instantiateViewController(withIdentifier: "placeDescriptionPopUpVcID") as! PlaceDescriptionPopUpViewController
        self.addChildViewController(popUpVc)
        popUpVc.view.frame = self.view.frame
        self.view.addSubview(popUpVc.view)
        popUpVc.didMove(toParentViewController: self)
        
        popUpVc.placeDescriptionTextView.text = detailPlace?.placeDescription
        
    }
    
   
    @IBAction func savePlace(_ sender: Any) {
        
        
        //to do: insert into saved places locally and on server
        //check if place visted
        //change icon of visted
        
        
        //saved places needs to be fixed. maybe it could simply store "random" arrays
        if Storage.shared.savedPlaces.contains(where: {$0.placeName == placeNameLabel.text!}) {
            
            let foundIndex = "\(Storage.shared.savedPlaces.index(where: {$0.placeName == placeNameLabel.text!}))"
            print(foundIndex)
            
            let url = "https://emalocalserver.herokuapp.com/savedPlaces/\(foundIndex)"
            
            Alamofire.request(url, method: .delete).validate(statusCode: 200..<600).responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print("JSON: \(json)")
                    self.saveButton.setImage(#imageLiteral(resourceName: "unvisted_icon"), for: UIControlState.normal)
                case .failure(let error):
                    print(error)
                }
            }
            
        } else {
            print("Params to send: \(savedPlaceParams)")
            
            let params = [
                "newPlace": savedPlaceParams
            ]
            
            Alamofire.request("https://emalocalserver.herokuapp.com/savedPlaces", method: .post,parameters:savedPlaceParams,encoding:JSONEncoding.default,headers:nil).validate(statusCode: 200..<600).responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    
                    if(json["status"] == "valid"){
                        
                        self.saveButton.setImage(#imageLiteral(resourceName: "visited_icon"), for: UIControlState.normal)
                        
                        /*
                        let newBool = self.savedPlaceParams["saved"] as! String == "true" ? true:false
                        let newRating = self.savedPlaceParams["placeRating"] as! String
                        let newPrice = self.savedPlaceParams["placePriceLevel"] as! String == "N/A" ? 0:(self.savedPlaceParams["placePriceLevel"] as! String).count
                        
                        //optional value gets unwrapped here for some reason...
                        let savedPlace = Place(placeName: self.savedPlaceParams["placeName"] as! String, placeAddress: self.savedPlaceParams["placeAddress"] as! String, placeRating: Float(newRating.count), placePriceLevel: newPrice, placeDescription: self.savedPlaceParams["placeDescription"] as! String, placeType: self.savedPlaceParams["placeType"] as! String, placeLatitude: self.savedPlaceParams["placeLatitude"] as! CLLocationDegrees, placeLongitude: self.savedPlaceParams["placeLongitude"] as! CLLocationDegrees, saved: newBool)
                        
                        Storage.shared.savedPlaces.append(savedPlace)
                        */
                    }
                    else{
                        self.saveButton.setImage(#imageLiteral(resourceName: "unvisted_icon"), for: UIControlState.normal)
                        print(json)
                    }
                    
                case .failure(let error):
                    
                    print(error)
                }
            }
        }
        
        
    }
    
    @IBAction func fireCompass(_ sender: Any) {
        
       //take me there logic
       //set desintation coordinates and
       // segue to compass vc
        
        
        performSegue(withIdentifier: "goToCompass", sender: self)
        
    
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "goToCompass"){
            let destinationVC = segue.destination as? CompassViewController
            
            destinationVC?.destinationLat = (detailPlace?.placeLatitude)!
            destinationVC?.destinationLong = (detailPlace?.placeLongitude)!
            destinationVC?.destinationName = (detailPlace?.placeName)!
            
            let currentLocation = CLLocation(latitude: userLat, longitude: userLong)
            
            //might have to change this to detailPlace.lat and lng or smth
            let destination = CLLocation(latitude: lat, longitude: lng)
            
            let distance = (currentLocation.distance(from: destination))
            
            //destinationVC?.distanceLabel.text = "\(distance)"
            
        }
    }
    
    
    
}

