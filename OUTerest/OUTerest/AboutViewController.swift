//
//  AboutViewController.swift
//  OUTerest
//
//  Created by Adem Hadrovic on 5/25/18.
//  Copyright © 2018 DATTeam. All rights reserved.
//

import UIKit




class AboutViewController: UIViewController {
    
    @IBInspectable var dayColor: UIColor = UIColor.clear
    @IBInspectable var nightColor: UIColor = UIColor.clear
    
    
    @IBOutlet weak var madeByTitleLabel: DayNightLabel!
    
    @IBOutlet weak var disclaimerTitleLabel: DayNightLabel!
    
    @IBOutlet weak var disclaimer1TextView: UITextView!
    
    @IBOutlet weak var disclaimer2TextView: UITextView!
    
    @IBOutlet weak var copyRightLabel: UITextView!

    @IBOutlet weak var copyRightTitleLabel: DayNightLabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
