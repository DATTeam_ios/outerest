//
//  MapViewController.swift
//  compass
//
//  Created by Federico Zanetello on 23/04/2017.
//  Copyright © 2017 Kimchi Media. All rights reserved.
//  Modified by Adem Hadrovic and Dalila Isanovic


import UIKit
import MapKit

class MapViewController2: UIViewController {
  var delegate: MapViewControllerDelegate!
  
    var destinationLat:CLLocationDegrees = 0.0
    var destinationLong:CLLocationDegrees = 0.0
    
    @IBOutlet weak var mapView: MKMapView!
  
  @IBAction func cancelTap(_ sender: UIBarButtonItem) {
    self.dismiss(animated: true, completion: nil)
  }
  
  @IBAction func resetTap(_ sender: UIBarButtonItem) {
    delegate.update(location: CLLocation(latitude: 90, longitude: 0))
    self.dismiss(animated: true, completion: nil)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    mapView.showsUserLocation = true
    if #available(iOS 9, *) {
      mapView.showsScale = true
      mapView.showsCompass = true
    }
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MapViewController2.didTap(_:)))
    mapView.addGestureRecognizer(gestureRecognizer)
  }

    @objc public func didTap(_ gestureRecognizer: UIGestureRecognizer) {
    
    /*
    let location = gestureRecognizer.location(in: mapView)
    let coordinate = mapView.convert(location, toCoordinateFrom: mapView)
    */
    
    delegate.update(location: CLLocation(latitude: destinationLat, longitude: destinationLong))
    print("testing lat: \(destinationLat)")
    print("testing lat: \(destinationLong)")
        
    self.dismiss(animated: true, completion: nil)
  }
}


