//
//  MainUiViewController.swift
//  OUTerest
//
//  Created by Adem Hadrovic on 5/3/18.
//  Copyright © 2018 DATTeam. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Alamofire
import SwiftyJSON


class MainUiViewController: UIViewController{
    
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var placesClient: GMSPlacesClient!
    var likelyPlaces: [GMSPlace] = []
    var googleInterests: [String] = []
    
    @IBOutlet weak var dailyTipLabel: UILabel!
    
    var tips = ["Always carry cash with you.","Drink lots of water"]
    
    let defaultLocation = CLLocation(latitude: 43.8597248, longitude: 18.4290523)
    
    
    func populateGoogleInterests(selectedInterests:[String]) -> Void{
        
        var translatedInterests:[String] = []
        
        if Storage.shared.googleInterests.isEmpty {
            for interest in selectedInterests {
                
                if(interest == "food"){
                    translatedInterests.append(contentsOf: Storage.shared.foodList)
                }
                else if(interest == "cafe"){
                    translatedInterests.append(contentsOf: Storage.shared.coffeeList)
                }
                else if(interest == "bar"){
                    translatedInterests.append(contentsOf: Storage.shared.clubList)
                }
                else if(interest == "bank"){
                    translatedInterests.append(contentsOf: Storage.shared.bankList)
                }
                else if(interest == "shopping"){
                    translatedInterests.append(contentsOf: Storage.shared.shoppingList)
                }
                else if(interest == "transport"){
                    translatedInterests.append(contentsOf: Storage.shared.transportList)
                }
                else if(interest == "lodging"){
                    translatedInterests.append(contentsOf: Storage.shared.hotelList)
                }
                else if(interest == "hospital"){
                    translatedInterests.append(contentsOf: Storage.shared.hospitalList)
                }
                else{
                    translatedInterests.append("")
                }
            }
            
            Storage.shared.googleInterests = translatedInterests
        }
        
        else{
            return
        }
        
    }
    
    
    override func viewDidLoad() {
        
        let randomIndex = Int(arc4random_uniform(UInt32(tips.count)))
        
        dailyTipLabel.text = tips[randomIndex]
        
        super.viewDidLoad()
        
        // Initialize the location manager.
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
        locationManager.delegate = self as? CLLocationManagerDelegate
        
        placesClient = GMSPlacesClient.shared()
        
        
        populateGoogleInterests(selectedInterests: Storage.shared.selectedIntrests )
        getLikelyPlaces()
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == "toFindPlacesVC"){
            let controller = segue.destination as? FindPlacesTableViewController
            controller?.setLikelyPlaces(inputArray: self.likelyPlaces)
            
            controller?.userLat = (locationManager.location?.coordinate.latitude)!
            controller?.userLong = (locationManager.location?.coordinate.longitude)!
        }
        
        if(segue.identifier == "toARVC"){
            //do something
        }
        
    
        
        
        
        //TO DO: only have places segued to the find places view
        
    }
    

    func getLikelyPlaces() {
        // Clean up from previous sessions.
        likelyPlaces.removeAll()
        
        placesClient.currentPlace(callback: { (placeLikelihoods, error) -> Void in
            if let error = error {
                // TODO: Handle the error.
                print("Current Place error: \(error.localizedDescription)")
                return
            }
            
            // Get likely places and add to the list and create markers on the map
            
            var placesParams = ["storedPlaces":[]] as [String:Any]
            
            if let likelihoodList = placeLikelihoods {
                for likelihood in likelihoodList.likelihoods {
                    
                    for interest in Storage.shared.googleInterests{
                        if likelihood.place.types.contains(interest){
                            let place = likelihood.place
                            self.likelyPlaces.append(place)
                            Storage.shared.storedPlaces.append(place)
                            print(place)
                            
                        }
                    }
                }
                
                placesParams["storedPlaces"] = Storage.shared.storedPlaces
                
            }
            /*
            Alamofire.request("https://emalocalserver.herokuapp.com/places", method: .post,parameters:placesParams,encoding:JSONEncoding.default,headers:nil).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    
                    print("saved places: \(json)")
                    self.performSegue(withIdentifier: "toFindPlacesVC", sender: "Any")
                    
                case .failure(let error):
                    print(error)
                }
            }
             */
            
            
        })
    }
    
    
}
