//
//  CompassViewController.swift
//  OUTerest
//
//  Created by Adem Hadrovic on 6/11/18.
//  Copyright © 2018 DATTeam. All rights reserved.
//

import UIKit

import GoogleMaps
import GooglePlaces
import MapKit
import CoreLocation


class CompassViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    var delegate: MapViewControllerDelegate!
    
    var destinationLat:CLLocationDegrees = 0.0
    var destinationLong:CLLocationDegrees = 0.0
    var destinationName:String = "Test Destination"
    
    var locationManager2 = CLLocationManager()
    //var currentLocation: CLLocation?
    var distance: CLLocationDistance?
    
    @IBOutlet weak var destinationNameLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
    
    let locationDelegate = LocationDelegate()
    var latestLocation: CLLocation? = nil
    var yourLocationBearing: CGFloat { return latestLocation?.bearingToLocationRadian(self.yourLocation) ?? 0 }
    var yourLocation: CLLocation {
        get { return UserDefaults.standard.currentLocation }
        set { UserDefaults.standard.currentLocation = newValue }
    }
    
    let locationManager: CLLocationManager = {
        $0.requestWhenInUseAuthorization()
        $0.desiredAccuracy = kCLLocationAccuracyBest
        $0.startUpdatingLocation()
        $0.startUpdatingHeading()
        return $0
    }(CLLocationManager())
    
    private func orientationAdjustment() -> CGFloat {
        let isFaceDown: Bool = {
            switch UIDevice.current.orientation {
            case .faceDown: return true
            default: return false
            }
        }()
        
        let adjAngle: CGFloat = {
            switch UIApplication.shared.statusBarOrientation {
            case .landscapeLeft:  return 90
            case .landscapeRight: return -90
            case .portrait, .unknown: return 0
            case .portraitUpsideDown: return isFaceDown ? 180 : -180
            }
        }()
        return adjAngle
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.update(location: CLLocation(latitude: destinationLat, longitude: destinationLong))
        
        locationManager2 = CLLocationManager()
        locationManager2.desiredAccuracy = kCLLocationAccuracyBest
        //locationManager.distanceFilter = 50
        locationManager2.startUpdatingLocation()
        locationManager2.delegate = self as? CLLocationManagerDelegate
       
        destinationNameLabel.text = destinationName
        
        var startLocation = locationManager2.location!
        let desination = CLLocation(latitude: destinationLat, longitude: destinationLong)
        
        distanceLabel.text = String(startLocation.distance(from: desination).rounded())
        
        var currentLocation = locationManager2.location! {
            didSet{
                
                //to do: change sig figures in distance (00.00)
                distance = currentLocation.distance(from: desination)
                distanceLabel.text = "\(String(describing: distance))"
                print("Current Location: \(currentLocation)")
                print("Current Distance to Dest.: \(String(describing: distance))")
                
            }
        }
        
        print("Current Distance: \(currentLocation.distance(from: desination))")
        
        locationManager.delegate = locationDelegate
        
        
        locationDelegate.locationCallback = { location in
            self.latestLocation = location
        }
        
        locationDelegate.headingCallback = { newHeading in
            
            func computeNewAngle(with newAngle: CGFloat) -> CGFloat {
                let heading: CGFloat = {
                    let originalHeading = self.yourLocationBearing - newAngle.degreesToRadians
                    switch UIDevice.current.orientation {
                    case .faceDown: return -originalHeading
                    default: return originalHeading
                    }
                }()
                
                return CGFloat(self.orientationAdjustment().degreesToRadians + heading)
            }
            
            UIView.animate(withDuration: 0.5) {
                let angle = computeNewAngle(with: CGFloat(newHeading))
                self.imageView.transform = CGAffineTransform(rotationAngle: angle)
            }
        }
        
        //let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(CompassViewController2.showMap))
        //view.addGestureRecognizer(tapGestureRecognizer)
        
        
    }
    /*
    @objc func showMap() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mapViewController = storyboard.instantiateViewController(withIdentifier: "MapViewController2")
        
        ((mapViewController as? UINavigationController)?.viewControllers.first as? MapViewController2)?.delegate = self as MapViewControllerDelegate
        self.present(mapViewController, animated: true, completion: nil)
        
        
    }
 */
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func showARMenuPopUp(_ sender: UIButton){
        
        let popUpVc = UIStoryboard(name:"Main",bundle:nil).instantiateViewController(withIdentifier: "arMenuVcID") as! ARMenuViewController
        
        self.addChildViewController(popUpVc)
        popUpVc.view.frame = self.view.frame
        self.view.addSubview(popUpVc.view)
        popUpVc.didMove(toParentViewController: self)
        
    }

}

extension CompassViewController: MapViewControllerDelegate {
    func update(location: CLLocation) {
        yourLocation = location
        print("testing lat: \(destinationLat)")
        print("testing lat: \(destinationLong)")
    }
}
