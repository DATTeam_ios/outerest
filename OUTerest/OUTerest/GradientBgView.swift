//
//  GradientBgView.swift
//  OUTerest
//
//  Created by Adem Hadrovic on 4/28/18.
//  Copyright © 2018 DATTeam. All rights reserved.
//

import UIKit

class GradientBgView: UIView{
    
    //Day Colors
    @IBInspectable var startColor: UIColor = UIColor.clear
    @IBInspectable var endColor: UIColor = UIColor.clear
    
    //Early Morning Colors
    @IBInspectable var earlyMorningStartColor: UIColor = UIColor.clear
    @IBInspectable var earlyMorningEndColor: UIColor = UIColor.clear
    
    //Morning Colors
    @IBInspectable var morningStartColor: UIColor = UIColor.clear
    @IBInspectable var morningEndColor: UIColor = UIColor.clear
    
    //Evening/Akšam Colors
    @IBInspectable var eveningStartColor: UIColor = UIColor.clear
    @IBInspectable var eveningEndColor: UIColor = UIColor.clear
    
    //Night Colors
    @IBInspectable var nightStartColor: UIColor = UIColor.clear
    @IBInspectable var nightEndColor: UIColor = UIColor.clear
    
    
    //TO DO: change colors of BG gradient based on time of day
    override func draw(_ rect: CGRect) {
        let currentHour = Calendar.current.component(.hour,from:Date())
        
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = CGRect(x: CGFloat(0),
                                y: CGFloat(0),
                                width: superview!.frame.size.width,
                                height: superview!.frame.size.height)
        
        
        if (currentHour >= 3 && currentHour < 7) {
            gradient.colors = [earlyMorningStartColor.cgColor, earlyMorningEndColor.cgColor]
        }
        else if(currentHour >= 7 && currentHour < 12){
            gradient.colors = [morningStartColor.cgColor, morningEndColor.cgColor]
        }
        else if(currentHour >= 12 && currentHour < 17){
            gradient.colors = [startColor.cgColor, endColor.cgColor]
        }
            
        else if(currentHour >= 17 && currentHour < 19){
            gradient.colors = [eveningStartColor.cgColor, eveningEndColor.cgColor]
        }
        else if(currentHour >= 19 || currentHour < 3){
            gradient.colors = [nightStartColor.cgColor, nightEndColor.cgColor]
        }
        else{
            gradient.colors = [startColor.cgColor, endColor.cgColor]
        }
        

        //gradient.colors = [nightStartColor.cgColor, nightEndColor.cgColor]
        
        gradient.zPosition = -1
        layer.addSublayer(gradient)
        
    }
    
    
    
}
