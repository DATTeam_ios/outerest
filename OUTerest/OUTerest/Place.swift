//
//  Place.swift
//  OUTerest
//
//  Created by Adem Hadrovic on 5/11/18.
//  Copyright © 2018 DATTeam. All rights reserved.
//

import Foundation
import GooglePlaces

class Place  {
    
    //will have to add image, once we have the type narrowed down
    
    let placeName: String
    let placeAddress: String
    let placeType:String
    let placeRating:String
    let placePriceLevel:String
    let placeDescription:String
    let placeLatitude:CLLocationDegrees
    let placeLongitude:CLLocationDegrees
    var saved:Bool
    
    
    static func convertRatingToStars (inputRating: Float) -> String {
        
        var rating: String = ""
        
        if(inputRating <= 5.0 && inputRating > 4.6){
            rating = "⭐⭐⭐⭐⭐"
        }
        else if(inputRating <= 4.4 && inputRating > 3.6){
            rating = "⭐⭐⭐⭐"
        }
        else if(inputRating <= 3.4 && inputRating > 2.6){
            rating = "⭐⭐⭐"
        }
        else if(inputRating <= 2.4 && inputRating > 1.6){
            rating = "⭐⭐"
        }
        else if(inputRating <= 1.4 && inputRating > 0.0){
            rating = "⭐"
        }
        else {
            rating = "N/A"
        }
        
        return rating
        
    }
    
    static func convertPriceToSign(inputPriceLevel:Int) -> String {
        
        var priceLevel: String
        
        //need to check this to see if it gets the
        switch inputPriceLevel{
        case 4:
            priceLevel = "$$$$"
        case 3:
            priceLevel = "$$$"
        case 2:
            priceLevel = "$$"
        case 1:
            priceLevel = "$"
        default:
            priceLevel = "N/A"
        }
        
        return priceLevel
        
    }
    
    
    init(placeName:String, placeAddress:String,placeRating:Float,placePriceLevel:Int,placeDescription: String, placeType:String,placeLatitude:CLLocationDegrees,placeLongitude:CLLocationDegrees,saved:Bool) {
        
        self.placeName = placeName
        self.placeAddress = placeAddress
        self.placeRating = Place.convertRatingToStars(inputRating: placeRating)
        self.placePriceLevel = Place.convertPriceToSign(inputPriceLevel: placePriceLevel)
        self.placeDescription = placeDescription
        self.placeType = placeType
        self.placeLatitude = placeLatitude
        self.placeLongitude = placeLongitude
        self.saved = saved
    }
    
    
    
    
}
