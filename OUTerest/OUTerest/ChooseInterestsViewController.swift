//
//  ChooseInterestsViewController.swift
//  OUTerest
//
//  Created by Adem Hadrovic on 4/17/18.
//  Copyright © 2018 DATTeam. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ChooseInterestViewController: UIViewController {
    
    @IBInspectable var dayColor: UIColor = UIColor.clear
    @IBInspectable var nightColor: UIColor = UIColor.clear
    
    var chosenInterests = Storage.shared.selectedIntrests
    var userDefaults = Storage.shared.userDefaults
    
    var wasVistied:Bool = false
    //to do, maybe set visited flag
    
    
    @IBOutlet weak var chooseRestaurantsButton: ChooseInterestUIButton!
    @IBOutlet weak var chooseCafesButton: ChooseInterestUIButton!
    @IBOutlet weak var chooseBarsButton: ChooseInterestUIButton!
    @IBOutlet weak var chooseBanksButton: ChooseInterestUIButton!
    @IBOutlet weak var chooseShoppingButton: ChooseInterestUIButton!
    @IBOutlet weak var chooseTransportButton: ChooseInterestUIButton!
    @IBOutlet weak var chooseHotelsButton: ChooseInterestUIButton!
    @IBOutlet weak var chooseHospitalsButton: ChooseInterestUIButton!
    
    
    @IBOutlet weak var restaurantLabel: UILabel!
    @IBOutlet weak var cafeLabel: UILabel!
    @IBOutlet weak var barLabel: UILabel!
    @IBOutlet weak var bankLabel: UILabel!
    @IBOutlet weak var shoppingLabel: UILabel!
    @IBOutlet weak var transportLabel: UILabel!
    @IBOutlet weak var hospitalLabel: UILabel!
    @IBOutlet weak var hotelLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        checkSelectedButton()
        print(chosenInterests)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func checkSelectedButton() -> Void {
        
        //might read from server here instead...

        for interest in Storage.shared.selectedIntrests{
            if(interest == "food"){
                chooseRestaurantsButton.setImage(#imageLiteral(resourceName: "restaurant_icon_selected"), for: UIControlState.normal)
                chooseRestaurantsButton.wasTapped = true
            }
            else if(interest == "cafe"){
                chooseCafesButton.setImage(#imageLiteral(resourceName: "cafe_icon_selected"), for: UIControlState.normal)
                chooseCafesButton.wasTapped = true
            }
            else if(interest == "bar"){
                chooseBarsButton.setImage(#imageLiteral(resourceName: "bar_icon_selected"), for: UIControlState.normal)
                chooseBarsButton.wasTapped = true
            }
            else if(interest == "bank"){
                chooseBanksButton.setImage(#imageLiteral(resourceName: "bank_icon_selected"), for: UIControlState.normal)
                chooseBanksButton.wasTapped = true
            }
            else if(interest == "shopping"){
                chooseShoppingButton.setImage(#imageLiteral(resourceName: "shopping_icon_selected"), for: UIControlState.normal)
                chooseShoppingButton.wasTapped = true
            }
            else if(interest == "transport"){
                chooseTransportButton.setImage(#imageLiteral(resourceName: "transport_icon_selected"), for: UIControlState.normal)
                chooseTransportButton.wasTapped = true
            }
            else if(interest == "lodging"){
                chooseHotelsButton.setImage(#imageLiteral(resourceName: "hotel_icon_selected"), for: UIControlState.normal)
                chooseHotelsButton.wasTapped = true
            }
            else if(interest == "hospital"){
                chooseHospitalsButton.setImage(#imageLiteral(resourceName: "hospital_icon_selected"), for: UIControlState.normal)
                chooseHospitalsButton.wasTapped = true
            }
            else{
                return
            }
        }
    }

    
    func addOrRemoveInterest(sender: ChooseInterestUIButton, initImage:UIImage, selectedImage:UIImage, interest:String,interestListArray:[String],interestListKey:String) -> Void {
        
        if(sender.wasTapped){
            
            sender.wasTapped = false
            sender.setImage(initImage, for: UIControlState.normal)
            for i in 0...Storage.shared.selectedIntrests.count-1{
                if(Storage.shared.selectedIntrests[i] == interest){
                    Storage.shared.selectedIntrests.remove(at: i)
                    
                    //for debugging: check current interests
                    print("Current Intrests: \(Storage.shared.selectedIntrests)")
                        break
                }
            }
            userDefaults.set(interestListArray, forKey: interestListKey)
        }
        else{
            
          Storage.shared.selectedIntrests.append(interest)
            
            //for debugging: check intrest value
            print(interest)
            //for debugging: check current interests
            print("Current Intrests: \(Storage.shared.selectedIntrests)")
            
            sender.wasTapped = true
            sender.setImage(selectedImage, for: UIControlState.normal)
            userDefaults.set(" ", forKey:interestListKey)
            userDefaults.removeObject(forKey:interestListKey)

        }
    }
    
    
    @IBAction func togglePlace(_ sender: ChooseInterestUIButton){
        
        switch sender.tag {
            case 1:
                addOrRemoveInterest(sender: sender, initImage: #imageLiteral(resourceName: "restaurant_icon"), selectedImage: #imageLiteral(resourceName: "restaurant_icon_selected"),interest: "food",interestListArray: Storage.shared.foodList, interestListKey: "restaurantList")
            case 2:
                addOrRemoveInterest(sender: sender, initImage: #imageLiteral(resourceName: "cafe_icon"), selectedImage: #imageLiteral(resourceName: "cafe_icon_selected"),interest:"cafe",interestListArray: Storage.shared.coffeeList,interestListKey: "cafeList")
            case 3:
                addOrRemoveInterest(sender: sender, initImage: #imageLiteral(resourceName: "bar_icon"), selectedImage: #imageLiteral(resourceName: "bar_icon_selected"),interest: "bar",interestListArray: Storage.shared.clubList,interestListKey: "barList")
            case 4:
                addOrRemoveInterest(sender: sender, initImage: #imageLiteral(resourceName: "bank_icon"), selectedImage: #imageLiteral(resourceName: "bank_icon_selected"),interest:"bank",interestListArray: Storage.shared.bankList,interestListKey: "bankList")
            case 5:
                addOrRemoveInterest(sender: sender, initImage: #imageLiteral(resourceName: "shopping_icon"), selectedImage: #imageLiteral(resourceName: "shopping_icon_selected"),interest: "shopping",interestListArray: Storage.shared.shoppingList,interestListKey: "shoppingList")
            case 6:
                addOrRemoveInterest(sender: sender, initImage: #imageLiteral(resourceName: "transport_icon"), selectedImage: #imageLiteral(resourceName: "transport_icon_selected"),interest: "transport", interestListArray: Storage.shared.transportList,interestListKey: "transportList")
            case 7:
                addOrRemoveInterest(sender: sender, initImage: #imageLiteral(resourceName: "hotel_icon"), selectedImage: #imageLiteral(resourceName: "hotel_icon_selected"),interest: "lodging",interestListArray: Storage.shared.hotelList,interestListKey: "hotelList" )
            case 8:
                addOrRemoveInterest(sender: sender, initImage: #imageLiteral(resourceName: "hospital_icon"), selectedImage: #imageLiteral(resourceName: "hospital_icon_selected"),interest: "hospital",interestListArray: Storage.shared.hospitalList,interestListKey: "hospitalList")
            default:
                return
        }
    
    }
    
    @IBAction func submitChosenIntersests(_ sender: Any) {
        
        
        let interestParams = [
            
            "chosenInterests": Storage.shared.selectedIntrests
            
            ] as [String:Any]
        
        print(interestParams)
        
        Alamofire.request("https://emalocalserver.herokuapp.com/interests", method: .post,parameters:interestParams,encoding:JSONEncoding.default,headers:nil).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                
                print("saved interests: \(json)")
                
                if(!self.wasVistied){
                    self.dismiss(animated: true, completion: nil);
                }
                else{
                    self.performSegue(withIdentifier: "toMainMenu", sender: "Any")
                }
                
                
            case .failure(let error):
                print(error)
            }
        }
        
    }
    
    
}
