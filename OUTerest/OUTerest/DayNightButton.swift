//
//  DayNightButton.swift
//  OUTerest
//
//  Created by Adem Hadrovic on 6/6/18.
//  Copyright © 2018 DATTeam. All rights reserved.
//

import UIKit

class DayNightButton: UIButton {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBInspectable var dayColor: UIColor = UIColor.clear
    @IBInspectable var nightColor: UIColor = UIColor.clear
    
    override func awakeFromNib() {
        
        
        let currentHour = Calendar.current.component(.hour,from:Date())
        
        if((currentHour >= 19 || currentHour < 3) || (currentHour >= 17 && currentHour < 19 )){
            
            self.setTitleColor(nightColor, for: UIControlState.normal)
            
        }
            
        else{
            
            self.setTitleColor(dayColor, for: UIControlState.normal)
            
        }
    }

}
