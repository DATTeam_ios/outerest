        //
//  AppDelegate.swift
//  OUTerest
//
//  Created by Adem Hadrovic on 4/15/18.
//  Copyright © 2018 DATTeam. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import ARKit
import Alamofire


import KudanAR.ARAPIKey

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        GMSServices.provideAPIKey("AIzaSyABtxks_hLl1AO0U-po52JokCmbw9k5hJE")
        GMSPlacesClient.provideAPIKey("AIzaSyBY7ytWYxOSG4xUzY7vljxxnamLchMSxzk")
        
        GMSPlacesClient.openSourceLicenseInfo()
        
        GMSServices.openSourceLicenseInfo()
    ARAPIKey.sharedInstance().setAPIKey("bGlfrb7a0jg4fcAeTMlrpj4suigFkahGuySZ8z8jT1BKXGYcdGqdTOWJ9fg+jYiqg1hYvXRNoQn5j2I8IpnFYuRWZVNh5/Vd+jGvzJZlqn1D0extCGq3m1my3Rt6hbT1U5tO18syyLgZyTbV6/TAFrD74+MOVu59wSAapt+fo1Wmzg3n1yVv91GjkEyRBWHi67A4P+2iQ7fL44wy1z8M3/SGIJJZmV1eVPRUxT2yb+0ahLvPCGqt4k+U97bet5Jf0XgPaHYcunIjNoQbi+7+iNRFhpxKsoaps0c37PMh1334eRuegX51eCbA0sAEQy9Ddd+Y+siEAX7ZJrrUNZKQT1AhBZH/byz9EwgTvMcu9v7dksCUOFXeZeiamycKPesD+6Qy0EnunjILZTDSixg7ztOW53ZFgNR6lbp9wFA4VzZGLVgUcxK+ksNHuZk5pZaG7mJ7HKV4dYDQQ2pr7B5wlObDKUjXD5uA/wT2oTt1YZwuoPlas0TNdBIXM7QvUh85ODWoaf6xwr9dGOl3HND6GQLCGWQfCHqAZhMN3fqGs0IrQwhBPvIGOFxAV/ymoW6GMnRE2fqmtmfFiQsWatgMiSF+KD2L6DYE8T4keRhA9vVT3HJjqdYvc5KnM3MFTuVn7LfV+QOCUySCYF/LdTAh23Om0V9zuvbi8fg/GmxQepo=")
        
        
        
        return true
    }
    
    //lock to portrait mode
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }
    

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

