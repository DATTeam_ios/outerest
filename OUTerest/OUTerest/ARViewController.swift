//
//  ARViewController.swift
//  OUTerest
//
//  Created by Adem Hadrovic on 6/12/18.
//  Copyright © 2018 DATTeam. All rights reserved.
//

import UIKit
import MapKit
import GooglePlaces
import GoogleMaps
import KudanAR

class ARViewController: ARCameraViewController {

    
    @IBOutlet weak var qMarkImage: UIImageView!
    @IBOutlet weak var explanationTextView: UITextView!
    
    @IBOutlet weak var menuButton: UIButton!
    //change these to current location to test AR ?
    var arLocLat:CLLocationDegrees = 43.8597248
    var arLocLong:CLLocationDegrees = 18.4290523
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        explanationTextView.layer.cornerRadius = 10
        
            menuButton.layer.cornerRadius = 5
        
        let locationManager = CLLocationManager()
        locationManager.startUpdatingLocation()
        
        let userLocation = locationManager.location!
        let arLocation = CLLocation(latitude: arLocLat, longitude: arLocLong)
        
        if (userLocation != arLocation){
            qMarkImage.isHidden = false
            explanationTextView.isHidden = false
        }
        else{
            //TO DO: fire AR camera
            qMarkImage.isHidden = true
            explanationTextView.isHidden = true
            print("TO DO: fire AR camera")
         
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func showARMenuPopUp(_ sender: UIButton){
        
        let popUpVc = UIStoryboard(name:"Main",bundle:nil).instantiateViewController(withIdentifier: "arMenuVcID") as! ARMenuViewController
        
        self.addChildViewController(popUpVc)
        popUpVc.view.frame = self.view.frame
        self.view.addSubview(popUpVc.view)
        popUpVc.didMove(toParentViewController: self)
        
    }
    
    override func setupContent()
    {
        // Setup code goes here
        
        // Initialise ArbiTrack.
        let arbiTrack = ARArbiTrackerManager.getInstance()
        arbiTrack?.initialise()
        
        
        // Initialise gyro placement.
        // Gyro placement positions content on a virtual floor plane.
        let gyroPlaceManager = ARGyroPlaceManager.getInstance()
        gyroPlaceManager?.initialise()
        
        // Setup the target node
        
        // Create a node to be used as the target.
        let targetImageNode = ARImageNode(image: UIImage(named:"Cow Target"))
        
        // Add it to the Gyro Placement Manager's world so that it moves with the device's Gyroscope.
        gyroPlaceManager?.world.addChild(targetImageNode)
        
        // Rotate and scale the node to ensure it is displayed correctly.
        targetImageNode?.rotate(byDegrees: 90, axisX: 1, y: 0, z: 0)
        targetImageNode?.rotate(byDegrees: 180, axisX: 0, y: 1, z: 0)
        
        targetImageNode?.scale(byUniform: 0.3)
        
        // Set the ArbiTracker's target node.
        arbiTrack?.targetNode = targetImageNode
        
        // Setup content with arbitrack
        
        // Create a node to be tracked
        let trackingImageNode = ARImageNode(image: UIImage(named:"Cow Tracking"))
        
        // Rotate the node to ensure it is displayed correctly.
        trackingImageNode?.rotate(byDegrees: 90, axisX: 1, y: 0, z: 0)
        trackingImageNode?.rotate(byDegrees: 180, axisX: 0, y: 1, z: 0)
        
        // Add the node as a child of the ArbiTracker's world.
        arbiTrack?.world.addChild(trackingImageNode)
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        let arbiTrack = ARArbiTrackerManager.getInstance()
        
        if (arbiTrack?.isTracking)!
        {
            arbiTrack?.stop()
            arbiTrack?.targetNode.visible = true
        }
            
        else
        {
            arbiTrack?.start()
            arbiTrack?.targetNode.visible = false
        }
    }

}
