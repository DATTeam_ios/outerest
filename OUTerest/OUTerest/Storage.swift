//
//  Storage.swift
//  OUTerest
//
//  Created by Adem Hadrovic on 5/13/18.
//  Copyright © 2018 DATTeam. All rights reserved.
//

import Foundation
import GoogleMaps
import GooglePlaces

class Storage{
    static let shared: Storage = Storage()
    
    var selectedIntrests:[String]
    var foodList:[String]
    var coffeeList:[String]
    let clubList:[String]
    let bankList:[String]
    let shoppingList:[String]
    let transportList:[String]
    let hotelList:[String]
    let hospitalList:[String]
    var googleInterests:[String]
    
    let userDefaults = UserDefaults.standard
    var storedPlaces:[GMSPlace]
    var savedPlaces:[Place]
    
    private init(){
        
        
        selectedIntrests = [String]()
        foodList = [/*"bakery","meal_delivery","meal_takeaway","restaurant",*/"food"]
        coffeeList = ["cafe"]
        clubList = ["bar"/*,"night_club"*/]
        bankList = ["atm","bank","finance"]
        shoppingList = [/*"bicycle_store","book_store","clothing_store","convenience_store","department_store","electronics_store","florist","furniture_store","gas_station","hardware_store","home_goods_store","jewelery_store","liquor_store","pet_store","shoe_store","shopping_mall","store","supermarket","grocery_or_supermarket"*/ "shopping_mall"]
        transportList = [/*"airport","bus_station","parking","subway_station","taxi_stand","train_station","transit_station","intersection"*/ "transit_station"]
        hotelList = [/*"campground","lodging","rv_park"*/"lodging"]
        hospitalList = [/*"dentist","doctor","hospital","pharmacy","health"*/"pharmacy"]
        
        googleInterests = []
        storedPlaces = []
        savedPlaces = []
        
    }
    
}
