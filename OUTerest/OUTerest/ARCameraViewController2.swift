//
//  ARCameraViewController.swift
//  OUTerest
//
//  Created by Adem Hadrovic on 6/19/18.
//  Copyright © 2018 DATTeam. All rights reserved.
//

import UIKit
import KudanAR


class ARCameraViewController2: ARCameraViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func setupContent()
    {
        // Setup code goes here
        
        // Initialise ArbiTrack.
        let arbiTrack = ARArbiTrackerManager.getInstance()
        arbiTrack?.initialise()
        
        
        // Initialise gyro placement.
        // Gyro placement positions content on a virtual floor plane.
        let gyroPlaceManager = ARGyroPlaceManager.getInstance()
        gyroPlaceManager?.initialise()
        
        // Setup the target node
        
        // Create a node to be used as the target.
        let targetImageNode = ARImageNode(image: UIImage(named:"Cow Target"))
        
        // Add it to the Gyro Placement Manager's world so that it moves with the device's Gyroscope.
        gyroPlaceManager?.world.addChild(targetImageNode)
        
        // Rotate and scale the node to ensure it is displayed correctly.
        targetImageNode?.rotate(byDegrees: 90, axisX: 1, y: 0, z: 0)
        targetImageNode?.rotate(byDegrees: 180, axisX: 0, y: 1, z: 0)
        
        targetImageNode?.scale(byUniform: 0.3)
        
        // Set the ArbiTracker's target node.
        arbiTrack?.targetNode = targetImageNode
        
        // Setup content with arbitrack
        
        // Create a node to be tracked
        let trackingImageNode = ARImageNode(image: UIImage(named:"Cow Tracking"))
        
        // Rotate the node to ensure it is displayed correctly.
        trackingImageNode?.rotate(byDegrees: 90, axisX: 1, y: 0, z: 0)
        trackingImageNode?.rotate(byDegrees: 180, axisX: 0, y: 1, z: 0)
        
        // Add the node as a child of the ArbiTracker's world.
        arbiTrack?.world.addChild(trackingImageNode)
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        let arbiTrack = ARArbiTrackerManager.getInstance()
        
        if (arbiTrack?.isTracking)!
        {
            arbiTrack?.stop()
            arbiTrack?.targetNode.visible = true
        }
            
        else
        {
            arbiTrack?.start()
            arbiTrack?.targetNode.visible = false
        }
    }

}


 
