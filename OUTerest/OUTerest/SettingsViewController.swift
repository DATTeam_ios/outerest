//
//  SettingsViewController.swift
//  OUTerest
//
//  Created by mini4 on 12.5.18..
//  Copyright © 2018. DATTeam. All rights reserved.
//

import UIKit

class SettingsViewController : UIViewController {

//Variables of the chosen interests, the three below are the default interests

let userDefaults = UserDefaults.standard

    let newPlacesSwitchKeyConstant = "newPlacesSwitch"
    let qrSwitchKeyConstant = "qrSwitch"
    
    @IBOutlet weak var newPlacesSwitch: UISwitch!
    @IBOutlet weak var qrSwitch: UISwitch!


    @IBAction func newPlacesSwitchAction(_ sender: UISwitch) {
        if (newPlacesSwitch.isOn == true){
            
            print("New Places notification is on")
        }
        else
        {
            print("New Places notification is off")
        }
    }
    
    @IBAction func qrSwitchAction(_ sender: UISwitch) {
        if (qrSwitch.isOn == true){
            
            print("QR notification is on")
        }
        else
        {
            print("QR notification is off")
        }
    }

override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    
    if(userDefaults.bool(forKey: "newPlacesSwitch")){
        newPlacesSwitch.isOn = true
    }
    else
    {
        newPlacesSwitch.isOn = false
    }
    
    if(userDefaults.bool(forKey: "qrSwitch")){
        qrSwitch.isOn = true
    }
    else
    {
        qrSwitch.isOn = false
    }
    
}

override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
}

override func viewWillDisappear(_ animated: Bool) {
    let userDefaults = UserDefaults.standard
    userDefaults.set(newPlacesSwitch.isOn, forKey: "newPlacesSwitch")
    userDefaults.set(qrSwitch.isOn, forKey: "qrSwitch")
}

}
