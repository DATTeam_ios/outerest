//
//  StartPageViewController.swift
//  OUTerest
//
//  Created by Adem Hadrovic on 5/25/18.
//  Copyright © 2018 DATTeam. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class StartPageViewController: UIViewController {

    @IBInspectable var dayColor: UIColor = UIColor.clear
    @IBInspectable var nightColor: UIColor = UIColor.clear
    
    @IBOutlet weak var titleLabel: DayNightLabel!
    @IBOutlet weak var subTitleLabel: DayNightLabel!
    @IBOutlet weak var promptLabel: DayNightLabel!
    @IBOutlet weak var creditsButton: UIButton!
    
    @IBAction func swipeToChooseInterestsOrNot(_ sender: Any) {
        
        Alamofire.request("https://emalocalserver.herokuapp.com/interests").responseJSON { response in
            switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    
                    if (json["chosenInterests"].isEmpty){
                            self.performSegue(withIdentifier: "goToChooseInterests", sender: self)
                        }
                        else{
                            self.performSegue(withIdentifier: "goToMainMenu", sender: self)
                        }
                
                case .failure(let error):
                    
                    self.performSegue(withIdentifier: "goToChooseInterests", sender: self)
                    
                    
                    print(error)
                }
            }
    }
    
    
    @IBAction func goToChooseInterestsOrNot(_ sender: Any) {
        
        if Storage.shared.selectedIntrests.isEmpty{
            performSegue(withIdentifier: "goToChooseInterests", sender: self)
        }
        else{
            performSegue(withIdentifier: "goToMainMenu", sender: self)
        }
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "goToChooseInterests" {
            
            let controller = segue.destination as? ChooseInterestViewController
            controller?.wasVistied = true;
            
        }
    
    }
    

}
