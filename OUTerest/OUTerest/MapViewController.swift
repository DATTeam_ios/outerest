//
//  MapViewController.swift
//  OUTerest
//
//  Created by Adem Hadrovic on 4/25/18.
//  Copyright © 2018 DATTeam. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class MapViewController: UIViewController, GMSMapViewDelegate {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var arButton: UIButton!
    
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var marker: GMSMarker!
    var mapView: GMSMapView!
    var placesClient: GMSPlacesClient!
    var likelyPlaces: [GMSPlace] = []
    
    var zoomLevel: Float = 18.0
    
    
    // A default location to use when location permission is not granted.
    let defaultLocation = CLLocation(latitude: 43.8597248, longitude: 18.4290523)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //backButton.layer.cornerRadius = 10
        
        // Initialize the location manager.
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
        
        placesClient = GMSPlacesClient.shared()
        
        // Create a map.
        let camera = GMSCameraPosition.camera(withLatitude: defaultLocation.coordinate.latitude,
                                              longitude: defaultLocation.coordinate.longitude,
                                              zoom: zoomLevel)
        mapView = GMSMapView.map(withFrame: view.bounds, camera: camera)
        mapView.settings.myLocationButton = true
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.isMyLocationEnabled = true
        
        
        
        // Creates a marker in the center of the map.
        marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: 43.8597248, longitude: 18.4290523)
        marker.icon = #imageLiteral(resourceName: "youarehere")
        marker.title = "Default Location"
        marker.snippet = "Marker Test"
        marker.map = mapView
        
        // Add the map to the view, hide it until we've got a location update.
        view.addSubview(mapView)
        mapView.isHidden = true
        
        listLikelyPlaces()
        
        do {
            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "mapstyle", withExtension: "json") {
                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        
    }
    func listLikelyPlaces() {
        // Clean up from previous sessions.
        likelyPlaces.removeAll()
        
        /*
        placesClient.currentPlace(callback: { (placeLikelihoods, error) -> Void in
            if let error = error {
                // TODO: Handle the error.
                print("Current Place error: \(error.localizedDescription)")
                return
            }
            
            // Get likely places and add to the list and create markers on the map
            
            if let likelihoodList = placeLikelihoods {
                for likelihood in likelihoodList.likelihoods {
                    //get the places based on chosen interests
                    for interest in Storage.shared.selectedIntrests{
                        if likelihood.place.types.contains(interest){
                            
                            let place = likelihood.place
                            self.likelyPlaces.append(place)
                            let marker = GMSMarker(position: (place.coordinate))
                            marker.title = place.name
                            marker.snippet = place.formattedAddress
                            marker.map = self.mapView
                            marker.icon = GMSMarker.markerImage(with: .purple)
                            marker.isFlat = true
                            
                            if(likelihood.place.types.contains("food")){
                                marker.icon = #imageLiteral(resourceName: "restaurant_icon_selected")
                            }
                            if(likelihood.place.types.contains("cafe")){
                                marker.icon = #imageLiteral(resourceName: "cafe_icon_selected")
                            }
                            if(likelihood.place.types.contains("bar")){
                                marker.icon = #imageLiteral(resourceName: "bar_icon_selected")
                            }
                            if(likelihood.place.types.contains("bank") || likelihood.place.types.contains("atm")){
                                marker.icon = #imageLiteral(resourceName: "bank_icon_selected")
                            }
                            if(likelihood.place.types.contains("shopping") || likelihood.place.types.contains("shopping_mall")){
                                marker.icon = #imageLiteral(resourceName: "shopping_icon_selected")
                            }
                            if(likelihood.place.types.contains("transport") || likelihood.place.types.contains("transit_station")){
                                marker.icon = #imageLiteral(resourceName: "transport_icon_selected")
                            }
                            if(likelihood.place.types.contains("lodging")){
                                marker.icon = #imageLiteral(resourceName: "hotel_icon_selected")
                            }
                            if(likelihood.place.types.contains("hospital")){
                                marker.icon = #imageLiteral(resourceName: "hospital_icon_selected")
                            }
                            
                            print(place)
                        }
                    }
                }
            }
        })
        */
        for place in Storage.shared.storedPlaces{
            /*
            for interest in Storage.shared.selectedIntrests{
                if place.types.contains(interest){
                */
                    //let place = likelihood.place
                    self.likelyPlaces.append(place)
                    let marker = GMSMarker(position: (place.coordinate))
                    marker.title = place.name
                    marker.snippet = place.formattedAddress
                    marker.map = self.mapView
                    marker.icon = GMSMarker.markerImage(with: .purple)
                    marker.isFlat = true
                    
                    if(place.types.contains("food")){
                        marker.icon = #imageLiteral(resourceName: "restaurant_icon_selected")
                    }
                    if(place.types.contains("cafe")){
                        marker.icon = #imageLiteral(resourceName: "cafe_icon_selected")
                    }
                    if(place.types.contains("bar")){
                        marker.icon = #imageLiteral(resourceName: "bar_icon_selected")
                    }
                    if(place.types.contains("bank") || place.types.contains("atm")){
                        marker.icon = #imageLiteral(resourceName: "bank_icon_selected")
                    }
                    if(place.types.contains("shopping") || place.types.contains("shopping_mall")){
                        marker.icon = #imageLiteral(resourceName: "shopping_icon_selected")
                    }
                    if(place.types.contains("transport") || place.types.contains("transit_station")){
                        marker.icon = #imageLiteral(resourceName: "transport_icon_selected")
                    }
                    if(place.types.contains("lodging")){
                        marker.icon = #imageLiteral(resourceName: "hotel_icon_selected")
                    }
                    if(place.types.contains("hospital")){
                        marker.icon = #imageLiteral(resourceName: "hospital_icon_selected")
                    }
                    
                    print(place)
                //}
            //}
        }
        
    }
    
}

// Delegates to handle events for the location manager.
extension MapViewController: CLLocationManagerDelegate {
    
    // Handle incoming location events.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
        print("Location: \(location)")
        
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                              longitude: location.coordinate.longitude,
                                              zoom: zoomLevel)
        
        if mapView.isHidden {
            mapView.isHidden = false
            mapView.delegate = self
            mapView.camera = camera
        } else {
            mapView.animate(to: camera)
        }
        
        
    }
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            
            print("Location access was restricted.")
            
        case .denied:
            
            print("User denied access to location.")
            // Display the map using the default location.
            
            mapView.isHidden = false
        case .notDetermined:
            
            currentLocation = CLLocation(latitude: 43.8597248, longitude: 18.4290523)
            
            print("Location status not determined.")
            
        case .authorizedAlways:
            
            locationManager.startUpdatingLocation()
            currentLocation = locationManager.location
            
            print("Location status is OK.")
            
        case .authorizedWhenInUse:
            
            locationManager.startUpdatingLocation()
            currentLocation = locationManager.location
            
            print("Location status is OK.")
            
        }
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
    
    // Set the status bar style to complement night-mode.
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        //when a marker is tapped, the places details will open
        
        //first the place has to be matched with the stored array
        let latitude = marker.position.latitude
        let longitude = marker.position.longitude
        var chosenPlace:Place
        
        for place in likelyPlaces {
            if (place.coordinate.latitude == latitude && place.coordinate.longitude == longitude){
                
                //will have to change place type - maybe I will write a filter function? It shouldn't be types[0]
                
                chosenPlace = Place(placeName: place.name, placeAddress: place.formattedAddress!, placeRating: place.rating, placePriceLevel: place.priceLevel.rawValue, placeDescription: place.description, placeType: place.types[0], placeLatitude: place.coordinate.latitude, placeLongitude: place.coordinate.longitude,saved:false)
                
                
                showPlaceDetailsPopup(chosenPlace: chosenPlace)
                break
                
            }
        }
        
        return true
    }
    
    func showPlaceDetailsPopup(chosenPlace:Place)->Void{
        
        //backButton.isHidden = true
        //arButton.isHidden = true
        
        let popUpVc = UIStoryboard(name:"Main",bundle:nil).instantiateViewController(withIdentifier: "placeDetailVcID") as! PlaceDetailViewController
        
        self.addChildViewController(popUpVc)
        popUpVc.view.frame = self.view.frame
        self.view.addSubview(popUpVc.view)
        popUpVc.didMove(toParentViewController: self)
        
        popUpVc.userLat = (currentLocation?.coordinate.latitude)!
        popUpVc.userLong = (currentLocation?.coordinate.longitude)!
        
        popUpVc.detailPlace = chosenPlace
    }
    
}

