//
//  FindPlacesTableViewController.swift
//  OUTerest
//
//  Created by Adem Hadrovic on 4/18/18.
//  Copyright © 2018 DATTeam. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Alamofire
import SwiftyJSON


class FindPlacesTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    var userLat:CLLocationDegrees = 43.8597248
    var userLong:CLLocationDegrees = 18.4290523
    
    var likelyPlaces: [GMSPlace] = []
    
    func setLikelyPlaces(inputArray:[GMSPlace]) -> Void {
        
        self.likelyPlaces = inputArray
    }
    
    func sortLikelyPlacesByCriteria(criteria:String)->Void{
        
        var sortedArray:[GMSPlace] = []
        
        switch criteria {
        case "rating":
            sortedArray = likelyPlaces.sorted(by: { $0.rating < $1.rating })
            break
        case "priceLevel":
            sortedArray = likelyPlaces.sorted(by: { $0.priceLevel.rawValue < $1.priceLevel.rawValue })
            break
        case "type":
            //for now, I will use the first type for sorting.
            sortedArray = likelyPlaces.sorted(by: { $0.types[0] < $1.types[0] })
            break
          
        default:
            //default. just return the normal array
            
            break
        }
        
        likelyPlaces = sortedArray
        
    
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return likelyPlaces.count
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "customCell")! as! PlaceCustomTableViewCell
        
        let googlePlace = likelyPlaces[indexPath.row]
        print(googlePlace)
        
        
        let place = Place(placeName: googlePlace.name, placeAddress: googlePlace.formattedAddress!, placeRating: googlePlace.rating, placePriceLevel: googlePlace.priceLevel.rawValue, placeDescription: googlePlace.description, placeType: googlePlace.types[0], placeLatitude: googlePlace.coordinate.latitude, placeLongitude: googlePlace.coordinate.longitude,saved:false)
        
        for p in Storage.shared.savedPlaces {
            if ((place.placeLatitude == p.placeLatitude) && (place.placeLongitude == p.placeLongitude)) {
                place.saved = true
                print("Visited Place: \(place)")
            }
        }
        
        //to do: read saved places from server and show checkmark
        Alamofire.request("https://emalocalserver.herokuapp.com/places_saved").responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                
                if(json["status"] == "found"){
                    for (_, subJson):(String, JSON) in json {
                        
                        let newLat = subJson["placeLatitude"].rawValue as! CLLocationDegrees
                        let newLng = subJson["placeLongitude"].rawValue as! CLLocationDegrees
                        
                        if ((newLat == place.placeLatitude) && (newLng == place.placeLongitude)) {
                            place.saved = true
                            cell.placeVisitedStampImageView.isHidden = false;
                            print("Visited Place: \(place)")
                        }
                        
    
                }
                    
                
                }
                
                self.tableView.reloadData()
                
                
            case .failure(let error):
                print(error)
            }
        }
        
        
        let currentLocation = CLLocation(latitude: userLat, longitude: userLong)
        let destination = CLLocation(latitude: place.placeLatitude, longitude: place.placeLongitude)
        
        let distance = currentLocation.distance(from: destination)
        
        cell.placeNameLabel.text! = place.placeName
        cell.placePriceLevelLabel.text! = place.placePriceLevel
        cell.placeRatingLabel.text! = place.placeRating
        cell.placeDistanceLabel.text! = "\(distance.rounded())"
        
        cell.currentPlace = place
        
        //will have to add image
        
        return cell
        
            
        //To Do: get distance from last recording location?
        //To Do: depending on the type: show interest cat image (?)
        //To Do: shrink and add visited bool / remove visited icon
        //To Do: Add price level to view cell
        //to do: handle phone number? //cell.placePhoneNumber = place.phoneNumber!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        tableView.delegate = self;
        tableView.dataSource = self;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func sortCells(_ sender: UIButton){
        
        switch sender.tag {
        case 1:
            sortLikelyPlacesByCriteria(criteria:"type")
            break
        case 2:
            sortLikelyPlacesByCriteria(criteria:"rating")

            break
        case 3:
            sortLikelyPlacesByCriteria(criteria:"priceLevel")
            
            break
        default:
            break
        }
        tableView.reloadData()
        
    }
    
    
    
    @IBAction func showPlaceDetailsPopUp(_ sender: UIButton){
        
        
    let cell =
        sender.superview.unsafelyUnwrapped.superview as! PlaceCustomTableViewCell
    
    let popUpVc = UIStoryboard(name:"Main",bundle:nil).instantiateViewController(withIdentifier: "placeDetailVcID") as! PlaceDetailViewController
        
        self.addChildViewController(popUpVc)
        popUpVc.view.frame = self.view.frame
        self.view.addSubview(popUpVc.view)
        popUpVc.didMove(toParentViewController: self)
    
        popUpVc.detailPlace = cell.currentPlace
    
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        /*
        let cell = (sender as AnyObject).superview.unsafelyUnwrapped
    */
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "customCell") as! PlaceCustomTableViewCell
        
        //this is a test segue for dem. purposes
        let controller = segue.destination as? PlaceDetailViewController
        
        let place = cell.currentPlace
        
        controller?.detailPlace = place
        controller?.userLat = userLat
        controller?.userLong = userLong
        controller?.lat = place.placeLatitude
        controller?.lng = place.placeLongitude
        
    }
    
}
